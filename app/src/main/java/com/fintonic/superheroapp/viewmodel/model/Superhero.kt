package com.fintonic.superheroapp.viewmodel.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = arrayOf("name", "realName"))
class Superhero {
    var id: Int = 0
    @SerializedName("name")
    var name: String? = ""
    @SerializedName("photo")
    var photo: String? = ""
    @SerializedName("realName")
    var realName: String? = ""
    @SerializedName("height")
    var height: String? = ""
    @SerializedName("power")
    var power: String? = ""
    @SerializedName("abilities")
    var abilities: String? = ""
    @SerializedName("groups")
    var groups: String? = ""
}

package com.fintonic.superheroapp.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel

import com.fintonic.superheroapp.data.UserRepository
import com.fintonic.superheroapp.data.network.Resource
import com.fintonic.superheroapp.viewmodel.model.Superhero

import javax.inject.Inject

/**
 * Created by seven on 12/07/2017.
 */

class MainViewModel @Inject
constructor(userRepository: UserRepository) : ViewModel() {

    val superheroes: LiveData<Resource<List<Superhero>>> = userRepository.all

}


package com.fintonic.superheroapp.view.base;

import android.app.Application;


import com.fintonic.superheroapp.R;

import java.util.List;

import dagger.ObjectGraph;

public class BaseApplication extends Application {

    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDependencyInjector();
    }


    private void initializeDependencyInjector() {
        objectGraph = ObjectGraph.create(new AppModule(this));
        objectGraph.inject(this);
        objectGraph.injectStatics();
    }

    public ObjectGraph plus(List<Object> modules) {
        if (modules == null) {
            throw new IllegalArgumentException(getString(R.string.invalid_module));
        }
        return objectGraph.plus(modules.toArray());
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }
}

package com.fintonic.superheroapp.view.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fintonic.superheroapp.R;
import com.fintonic.superheroapp.data.network.Resource;
import com.fintonic.superheroapp.view.base.fragment.DaggerFragment;
import com.fintonic.superheroapp.viewmodel.HeroesViewModelFactory;
import com.fintonic.superheroapp.viewmodel.MainViewModel;
import com.fintonic.superheroapp.viewmodel.model.Superhero;

import java.util.List;

import javax.inject.Inject;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends DaggerFragment {

    @Inject
    HeroesViewModelFactory factory;

    MainViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        viewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        viewModel.getSuperheroes().observe(this, new Observer<Resource<List<Superhero>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<Superhero>> listResource) {
                Log.d(getClass().getSimpleName(), "HOLA: " + listResource);
            }
        });

        return view;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    protected void refresh() {

    }
}

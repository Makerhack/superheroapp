package com.fintonic.superheroapp.view.base.fragment;

import android.arch.lifecycle.LifecycleFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MenuItem;

import com.fintonic.superheroapp.R;
import com.fintonic.superheroapp.view.base.ViewInterface;


public abstract class BaseFragment extends LifecycleFragment
        implements ViewInterface, SwipeRefreshLayout.OnRefreshListener {

    protected SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout = view.findViewById(R.id.refresh_zone);
        setupRefreshComponent();
    }

    private void setupRefreshComponent() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, android.R.color.black);
            swipeRefreshLayout.setEnabled(true);
        }
    }

    @Override
    public void showError(String message) {
        showMessage(message);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        refresh();
    }

    protected abstract void refresh();


    @Override
    public void showConnectionError() {
        if (getActivity() != null) {
            showMessage(R.string.cant_connect);
        }
    }

    @Override
    public void showParseError() {
        if (getActivity() != null) {
            showMessage(R.string.cant_parse);
        }
    }

    @Override
    public void showEmptyError() {
        if (getActivity() != null) {
            showMessage(R.string.no_object);
        }
    }

    @Override
    public void showMessage(int resourceId) {
        if (isAdded() && getView() != null) {
            Snackbar snackbar = Snackbar.make(getView(), resourceId, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    @Override
    public void showMessage(String message) {
        if (isAdded() && getView() != null) {
            Snackbar snackbar = Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    @Override
    public boolean hasLoadingView() {
        return swipeRefreshLayout != null;
    }


    @Override
    public void showLoad() {
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
    }

    @Override
    public void hideLoad() {
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(false));
    }

    protected void setPullToRefreshEnabled(boolean enabled) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setEnabled(enabled);
        }
    }


}

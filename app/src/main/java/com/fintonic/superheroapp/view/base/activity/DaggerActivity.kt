package com.fintonic.superheroapp.view.base.activity

import android.os.Bundle


import com.fintonic.superheroapp.view.base.BaseApplication

import dagger.ObjectGraph

abstract class DaggerActivity : BaseActivity() {
    private var activityScopeGraph: ObjectGraph? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
    }

    private fun injectDependencies() {
        val modules = modules
        modules.add(ActivityModule(this))
        activityScopeGraph = injectableApplication.plus(modules)
        inject(this)
    }

    private val injectableApplication: BaseApplication
        get() = application as BaseApplication

    fun inject(`object`: Any) {
        activityScopeGraph!!.inject(`object`)
    }

    protected abstract val modules: MutableList<Any>

}

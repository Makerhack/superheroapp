package com.fintonic.superheroapp.view;

public class Constants {
    public static final String DATABASE_NAME = "users_db";
    public static final int DATABASE_VERSION = 1;


    public static class UseCases {
        public static final String USE_CASE = "UseCase";
        public static final String FORCE_UPDATE = "ForceUpdate";
        public static final String PARAMS = "params";
        public static final int GET_USERS = 0;
        public static final int DELETE_USER = 1;
        public static final int FAV_USER = 2;
        public static final int GET_MORE_USERS = 3;
        public static final int GET_USER = 4;
        public static final int SEARCH_USERS = 5;
        public static final int FAVORITE_USERS = 6;
    }

    public class Errors {
        public static final int SERVER_ERROR = 1;
        public static final int EMPTY_RESPONSE = -1;
        public static final int ERROR_CANT_PARSE = -2;
        public static final int ERROR_SERVICE_UNAVAILABLE = 404;
        public static final int NO_USER = -3;
    }

    public class Keys {
        public static final String USER_ID = "userId";
        public static final String IS_FAV = "isFav";
        public static final String USER = "user";
        public static final String SEARCH_QUERY = "searchQuery";
        public static final String IS_SEARCH = "isSearch";
    }

    public class Actions {
        public static final String ACTION_DELETE_USER = "com.tuenti.tuentitestsantiago.ACTION_DELETE_USER";
        public static final String ACTION_FAV_USER = "com.tuenti.tuentitestsantiago.ACTION_FAV_USER";
        public static final String SEARCH_INTERNAL ="com.tuenti.tuentitestsantiago.SEARCH_INTERNAL";
    }
}

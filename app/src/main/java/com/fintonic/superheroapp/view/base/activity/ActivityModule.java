package com.fintonic.superheroapp.view.base.activity;

import android.app.Activity;
import android.content.Context;

import com.fintonic.superheroapp.view.base.annotations.ActivityContext;

import dagger.Module;
import dagger.Provides;

@Module(library = true)
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {

        this.activity = activity;
    }

    @Provides
    @ActivityContext
    Context provideActivityContext() {
        return activity;
    }
}
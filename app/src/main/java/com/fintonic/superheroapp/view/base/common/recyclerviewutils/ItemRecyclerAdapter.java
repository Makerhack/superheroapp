package com.fintonic.superheroapp.view.base.common.recyclerviewutils;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

public abstract class ItemRecyclerAdapter<T, VH extends ItemViewHolder<T>> extends RecyclerView.Adapter<VH> {
    protected final Context context;
    private final LayoutInflater mLayoutInflater;
    private OnItemClickListener<T> listener;
    private List<T> dataList = new LinkedList<>();

    public ItemRecyclerAdapter(Context context) {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void clear() {
        if (dataList != null) {
            dataList.clear();
        }
    }

    public List<T> getItems() {
        return dataList;
    }

    protected LayoutInflater getLayoutInflater() {
        return mLayoutInflater;
    }

    @Override
    public abstract VH onCreateViewHolder(ViewGroup viewGroup, int i);

    @Override
    public void onBindViewHolder(VH vh, int position) {
        T item = dataList.get(position);
        vh.bindItem(item, position, listener);
        onBindViewHolder(vh, position, item);
    }

    public abstract void onBindViewHolder(VH holder, int position, T item);

    public T getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void set(List<T> objectList) {
        if (objectList != null) {
            this.dataList.clear();
            this.dataList.addAll(objectList);
            notifyDataSetChanged();
        }
    }

    public void add(T object) {
        add(object, dataList.size());
    }

    public void add(T object, int position) {
        if (dataList != null) {
            this.dataList.add(position, object);
            notifyDataSetChanged();
        }
    }

    public void addAll(List<T> objectList) {
        addAll(objectList, dataList.size());
    }

    public void addAll(List<T> objectList, int position) {
        if (objectList != null) {
            this.dataList.addAll(position, objectList);
            notifyDataSetChanged();
        }
    }

    public void remove(T object) {
        if (object != null) {
            boolean removed = this.dataList.remove(object);
            Log.d(getClass().getSimpleName(), "removed: " + String.valueOf(removed));
            notifyDataSetChanged();
        }
    }

    public void removeAll(List<T> objectList) {
        if (objectList != null) {
            this.dataList.removeAll(objectList);
            notifyDataSetChanged();
        }
    }

    public void setOnItemClickListener(OnItemClickListener<T> listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener<T> {
        public void onItemClick(T item, View rowView, int position);
    }
}
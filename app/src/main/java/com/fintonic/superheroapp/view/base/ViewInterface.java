package com.fintonic.superheroapp.view.base;


import android.support.annotation.StringRes;

public interface ViewInterface {


    void showConnectionError();

    void showParseError();

    void showEmptyError();

    void showError(String message);

    void showMessage(@StringRes int resourceId);

    void showMessage(String message);

    void showLoad();

    void hideLoad();

    boolean hasLoadingView();

}

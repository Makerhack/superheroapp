package com.fintonic.superheroapp.view.ui.activity;

import com.fintonic.superheroapp.data.AppExecutors;
import com.fintonic.superheroapp.data.UserRepository;
import com.fintonic.superheroapp.data.local.SuperheroDao;
import com.fintonic.superheroapp.data.network.Webservice;
import com.fintonic.superheroapp.view.ui.fragment.MainActivityFragment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(library = true, complete = false,
        injects = {MainActivity.class, MainActivityFragment.class})

public class MainActivityModule {

    @Provides
    @Singleton
    public UserRepository providesUserRepository(Webservice webservice, SuperheroDao superheroDao,
                                                 AppExecutors appExecutors) {
        return new UserRepository(webservice, superheroDao, appExecutors);
    }

}

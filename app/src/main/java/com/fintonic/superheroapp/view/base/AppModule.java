package com.fintonic.superheroapp.view.base;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.persistence.room.Room;

import com.fintonic.superheroapp.data.UserRepository;
import com.fintonic.superheroapp.data.local.HeroesDatabase;
import com.fintonic.superheroapp.data.local.SuperheroDao;
import com.fintonic.superheroapp.data.network.LiveDataCallAdapterFactory;
import com.fintonic.superheroapp.data.network.Webservice;
import com.fintonic.superheroapp.viewmodel.HeroesViewModelFactory;
import com.fintonic.superheroapp.viewmodel.MainViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(library = true, injects = {BaseApplication.class})
public class AppModule {

    private Application application;

    public AppModule(Application application) {

        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Singleton
    @Provides
    Webservice provideWebService() {
        return new Retrofit.Builder()
                .baseUrl("https://api.myjson.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(Webservice.class);
    }

    @Singleton
    @Provides
    HeroesDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application, HeroesDatabase.class, "heroes.db").build();
    }

    @Singleton
    @Provides
    SuperheroDao provideUserDao(HeroesDatabase database) {
        return database.heroesDao();
    }

    @Provides
    @Singleton
    MainViewModel provideMainViewModel(UserRepository userRepository) {
        return new MainViewModel(userRepository);
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory provideFactory(MainViewModel mainViewModel) {
        return new HeroesViewModelFactory(mainViewModel);
    }
}

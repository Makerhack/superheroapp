package com.fintonic.superheroapp.view.base.common.recyclerviewutils;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Viewholder class that has a reference to the cell data element
 * Created by Santi on 10/04/2015.
 */
public abstract class ItemViewHolder<T> extends RecyclerView.ViewHolder {
    protected T item;
    protected int position;

    public ItemViewHolder(View itemView) {
        super(itemView);
    }

    public void bindItem(final T item, final int position, final ItemRecyclerAdapter.OnItemClickListener<T> listener) {
        this.item = item;
        this.position = position;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(item, itemView, position);
                }
            }
        });
    }
}

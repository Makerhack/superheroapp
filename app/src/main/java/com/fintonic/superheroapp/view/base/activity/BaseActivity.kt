package com.fintonic.superheroapp.view.base.activity

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity


abstract class BaseActivity : AppCompatActivity() {

    protected var homeButtonEnabled = true


    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        val supportActionBar = supportActionBar
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowHomeEnabled(true)
            if (shouldSetActionBarBack()) {
                supportActionBar.setDisplayHomeAsUpEnabled(homeButtonEnabled)
                supportActionBar.setHomeButtonEnabled(homeButtonEnabled)
            }
            if (shouldShowHomeIcon()) {
                supportActionBar.setDisplayUseLogoEnabled(true)
            }
        }
    }

    protected fun shouldSetActionBarBack(): Boolean {
        return true
    }

    protected fun shouldShowHomeIcon(): Boolean {
        return false
    }

}

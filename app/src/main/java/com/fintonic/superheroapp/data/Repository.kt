package com.fintonic.superheroapp.data

import android.arch.lifecycle.LiveData
import com.fintonic.superheroapp.data.network.Resource


/**
 * Inteface representing a data repository
 * Created by seven on 22/04/2017.
 */

interface Repository<T, K> {

    fun getById(userId: K): LiveData<Resource<T>>
    val all: LiveData<Resource<List<T>>>

}

package com.fintonic.superheroapp.data.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.fintonic.superheroapp.viewmodel.model.Superhero


/**
 * Created by seven on 11/07/2017.
 */
@Dao
interface SuperheroDao {
    @Insert(onConflict = REPLACE)
    fun save(user: Superhero)

    @Insert(onConflict = REPLACE)
    fun save(users: List<Superhero>)

    @Query("SELECT * FROM superhero WHERE id = :arg0")
    fun load(userId: String): LiveData<Superhero>

    @Query("SELECT * FROM superhero")
    fun loadAll(): LiveData<List<Superhero>>

    @Update
    fun updateHeroes(vararg users: Superhero)

    @Delete
    fun deleteUsers(vararg users: Superhero)

}

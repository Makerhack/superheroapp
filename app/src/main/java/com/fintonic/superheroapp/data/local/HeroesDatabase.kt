package com.fintonic.superheroapp.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

import com.fintonic.superheroapp.viewmodel.model.Superhero

@Database(entities = arrayOf(Superhero::class), version = 1)
abstract class HeroesDatabase : RoomDatabase() {
    abstract fun heroesDao(): SuperheroDao
}
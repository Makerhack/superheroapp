package com.fintonic.superheroapp.data.network;

import android.arch.lifecycle.LiveData;

import com.fintonic.superheroapp.viewmodel.model.Superhero;

import java.util.List;

import retrofit2.http.GET;

/**
 * Created by seven on 11/07/2017.
 */

public interface Webservice {

    @GET("bins/bvyob")
    LiveData<ApiResponse<List<Superhero>>> getSuperHeroes();
}

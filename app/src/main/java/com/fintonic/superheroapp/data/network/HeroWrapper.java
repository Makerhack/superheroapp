package com.fintonic.superheroapp.data.network;

import com.fintonic.superheroapp.viewmodel.model.Superhero;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by seven on 12/07/2017.
 */

public class HeroWrapper     {
    @SerializedName("superheroes")
    @Expose
    public List<Superhero> superheroes = null;
}

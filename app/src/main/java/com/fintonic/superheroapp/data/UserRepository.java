package com.fintonic.superheroapp.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fintonic.superheroapp.data.local.SuperheroDao;
import com.fintonic.superheroapp.data.network.ApiResponse;
import com.fintonic.superheroapp.data.network.NetworkBoundResource;
import com.fintonic.superheroapp.data.network.Resource;
import com.fintonic.superheroapp.data.network.Webservice;
import com.fintonic.superheroapp.viewmodel.model.Superhero;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Repository for quering user data
 * Created by seven on 22/04/2017.
 */

@Singleton
public class UserRepository implements Repository<Superhero, String> {
    private final Webservice webservice;
    private final SuperheroDao userDao;
    private final AppExecutors appExecutors;

    @Inject
    public UserRepository(Webservice webservice, SuperheroDao userDao, AppExecutors executor) {
        this.webservice = webservice;
        this.userDao = userDao;
        this.appExecutors = executor;
    }

    @NonNull
    public LiveData<Resource<Superhero>> getById(String userId) {
        return new NetworkBoundResource<Superhero, Superhero>(appExecutors) {
            @Override
            protected void saveCallResult(@NonNull Superhero item) {
                userDao.save(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable Superhero data) {
                return data == null;
            }

            @NonNull
            @Override
            protected LiveData<Superhero> loadFromDb() {
                return userDao.load(userId);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<Superhero>> createCall() {
                return new MutableLiveData<>();
            }
        }.asLiveData();
    }

    @NonNull
    public LiveData<Resource<List<Superhero>>> getAll() {
        return new NetworkBoundResource<List<Superhero>, List<Superhero>>(appExecutors) {
            @Override
            protected void saveCallResult(@NonNull List<Superhero> items) {
                userDao.save(items);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Superhero> data) {
                return data == null || data.isEmpty();
            }

            @NonNull
            @Override
            protected LiveData<List<Superhero>> loadFromDb() {
                return userDao.loadAll();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<Superhero>>> createCall() {
                return webservice.getSuperHeroes();
            }
        }.asLiveData();
    }
}
